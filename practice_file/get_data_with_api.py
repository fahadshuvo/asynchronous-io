import aiohttp
import asyncio

async def fetch_data(url):
  async with aiohttp.ClientSession() as session:
    async with session.get(url) as resp:
      if resp.status == 200:
        return await resp.json()
      else:
        print(f"Error fetching data from {url}: Status {resp.status}")
        return None

async def main():
  urls = [
      "https://api.example.com/data1", 
      "https://api.example.com/data2",
      "https://api.example.com/data3"
  ]

  tasks = [fetch_data(url) for url in urls]
  data_list = await asyncio.gather(*tasks)

  for data in data_list:
    if data:
      print(f"Data from {url}: {data['key1']}")

asyncio.run(main())
