import aiohttp
import asyncio
from bs4 import BeautifulSoup

async def scrape_website(url):
  async with aiohttp.ClientSession() as session:
    async with session.get(url) as resp:
      if resp.status == 200:
        html_content = await resp.text()
        soup = BeautifulSoup(html_content, 'html.parser')

        titles = soup.find_all('h1', class_='article-hero__h1')

        data = []
        for title in titles:
          data.append(title.text.strip())

        return data
      else:
        print(f"Error scraping data from {url}: Status {resp.status}")
        return None

async def main():
  url = "https://blog.google/products/fitbit/fitbit-charge-6-overview/"

  data = await scrape_website(url)

  if data:
    for item in data:
      print(item)
  else :
    print('none')

asyncio.run(main())
