import aiohttp
import aiofiles
import asyncio

async def download_file(url, filename):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            if resp.status == 200:
                async with aiofiles.open(f"downloads/{filename}", mode='wb') as f:
                    await f.write(await resp.read())
                    print(f"Downloaded {filename}")
            else:
                print(f"Error downloading {filename}: Status {resp.status}")

async def main():
    downloads = [
      ("https://wallpapercave.com/wp/RYgbsVe.jpg", 'img_1.jpg'),
      ("https://wallpapercave.com/wp/RYgbsVe.jpg", 'img_2.jpg'),
      ("https://wallpapercave.com/wp/RYgbsVe.jpg", 'img_3.jpg')
      ]

    tasks = [download_file(url, filename) for url, filename in downloads]
    await asyncio.gather(*tasks)

asyncio.run(main())
