import asyncio


async def coffee():
    print('coffee making')
    await asyncio.sleep(5)
    print('coffee is done')

async def toast():
    print('toast making')
    await asyncio.sleep(3)
    print('toast is done')


async def main():
    await asyncio.gather( coffee(), toast())
    print('breakfast is ready')


if __name__ == '__main__':
    asyncio.run(main())