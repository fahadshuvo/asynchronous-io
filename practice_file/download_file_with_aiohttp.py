import asyncio
import aiohttp        
import aiofiles

async def download_img():

    async with aiohttp.ClientSession() as session:
        url = "https://wallpapercave.com/wp/RYgbsVe.jpg"
        async with session.get(url) as resp:
            if resp.status == 200:
                file_path = 'downloads/a.jpg'
                async with aiofiles.open(file_path, mode='wb') as f :
                    await f.write(await resp.read())


asyncio.run(download_img())
            